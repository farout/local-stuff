This is my personal liguros overlay.

It is used for ebuilds that I use which are not in the current liguros tree. And also for testing ebuilds to integrate into the liguros tree. After incorporating the ebuilds into the tree, they will be removed from this overlay.
